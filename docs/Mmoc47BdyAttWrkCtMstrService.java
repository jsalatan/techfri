import java.sql.Connection;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

import jp.co.ulsystems.umlaut.dao.EntityNotFoundException;

public class Mmoc47BdyAttWrkCtMstrService extends Service {

    /** コネクター */
    private final Connection con = super.getConnection();

    /**
     * コンストラクタ
     */
    public Mmoc47BdyAttWrkCtMstrService() {
        super();
    }

    /**
     * コンストラクタ
     *
     * @param conn
     * データベース接続
     */
    public Mmoc47BdyAttWrkCtMstrService(Connection conn) {
        super(conn);
    }


    /**
     * 初期処理にドロップ内容を取得する.
     *
     * @param Value Valueのバリュー
     * @return BdyATTWrkCTMstrListValue のバリュー
     * @throws ApplicationException エラーが発生するとき
     */
    public Mmoc47BdyAttWrkCtMstrValue getDropdownContent(Value value)
        throws ApplicationException {

        Mmoc47BdyAttWrkCtMstrValue value = (Mmoc47BdyAttWrkCtMstrValue) value;

        // ラインコードのドロップ内容
        BdyAttWrkctMstrMgr mm059Mgr = new BdyAttWrkctMstrMgr(this.con);
        List<Map<String,Object>> mm059Result = mm059Mgr.queryLineCd(value.getZoneCd(),
                value.getLocaleLangCd());

        if (mm059Result == null || mm059Result.isEmpty()) {
            // ＥＲＲ０５１：{"ラインコード"} がマスターに存在しません
            ApplicationException ex = new ApplicationException();
            ex.addMessage("result|error.not.found.master.target|error.table.line_cd");
            throw ex;
        }

        List<String> dropLineCd = new ArrayList<String>();
        for (Map<String, Object> recordMap : mm059Result) {
            String lineCd = (String) recordMap.get("LINE_CD");
            dropLineCd.add(lineCd);
        }
        value.setDropLineCd(dropLineCd);

        // CT登録職場のドロップ内容
        BdyAttWrkctDtlMstrMgr mm060Mgr = new BdyAttWrkctDtlMstrMgr(this.con);
        List<Map<String,Object>> mm060Result = mm060Mgr.queryCtWorkplaceCd(value.getZoneCd(),
                value.getLocaleLangCd());

        if (mm060Result == null || mm060Result.isEmpty()) {
            // ＥＲＲ０５１：{"CT登録職場"} がマスターに存在しません
            ApplicationException ex = new ApplicationException();
            ex.addMessage("result|error.not.found.master.target|error.table.wrkct");
            throw ex;
        }

        List<LabelValueHolder> dropCtWorkplaceCd = new ArrayList<LabelValueHolder>();
        for (Map<String, Object> recordMap : mm060Result) {
            String ctWorkplaceCd = ConverterUtil.toString(recordMap.get("WORKPLACE_CD"));
            String ctWorkplaceName = ConverterUtil.toString(recordMap.get("WORKPLACE_NAME"));

            LabelValueHolder labelValue = new LabelValueHolder(
                    ctWorkplaceCd + " : " + ctWorkplaceName, ctWorkplaceCd);
            dropCtWorkplaceCd.add(labelValue);
        }
        value.setDropCtWorkPlaceCd(dropCtWorkplaceCd);

        return value;
    }

    /**
     * 検索ボタン処理
     *
     * @param value Valueのバリュー
     * @return BdyATTWrkCTMstrListValue のバリュー
     * @throws ApplicationException エラーが発生するとき
     */
    public Mmoc47BdyAttWrkCtMstrValue search(Value value)
        throws ApplicationException {

        Mmoc47BdyAttWrkCtMstrValue value = (Mmoc47BdyAttWrkCtMstrValue) value;

        String zoneCd = value.getZoneCd();
        String series = value.getSeries();
        String generation = value.getGeneration();
        String ctSpecCd = value.getCtSpec();
        String ppAreaCd = value.getPpArea();
        String lineCd = value.getLineCd();
        String ctWorkplaceCd = value.getCtWorkPlace();
        Date fromDate = value.getFromDate();
        String tempDstnctn = value.getTempCategory();

        String orderBy = getOrderByColumns(
                value.getSortCond1(), value.getSortOrd1(),
                value.getSortCond2(), value.getSortOrd2(),
                value.getSortCond3(), value.getSortOrd3(),
                value.getSortCond4(), value.getSortOrd4(),
                value.getSortCond5(), value.getSortOrd5());

        BdyAttWrkctMstrMgr mm059Mgr = new BdyAttWrkctMstrMgr(this.con);

        // 該当データの件数を取得する。
        int count = mm059Mgr.countRowData(zoneCd, series, generation,
                ctSpecCd, ppAreaCd, lineCd, ctWorkplaceCd, fromDate, tempDstnctn);

        /*
         * 次ページ、前ページの処理に検索ボタンが押下した時の検索結果（件数）と
         * 今回の検索結果（件数）が異なる場合、エラーにする
         */
        if(("prev".equals(value.getProcess()) || "next".equals(value.getProcess()))
                && count != value.getTotalCount()){
            // ＥＲＲ９９３：該当データは最新ではありません。再検索を行って下さい。
            throw new ApplicationException("result|error.page.data.not.latest");
        }
        value.setTotalCount(count);

        // HEADデータの取得 (page control sql)
        List<Map<String,Object>> rowData = null;

        rowData = mm059Mgr.getRowData(zoneCd, series, generation,
                ctSpecCd, ppAreaCd, lineCd, ctWorkplaceCd, fromDate, tempDstnctn, orderBy,
                value.getStartRow(), value.getEndRow());
        if (rowData == null || rowData.isEmpty()) {
            //「ＥＲＲ０４７：該当データはありません。」
            throw new ApplicationException("result|error.data.not.exist");
        }
        Mmoc47BdyAttWrkCtMstrValueUtil.transferRowDataToValue(rowData, value);

        // DETAILデータのラベルパターンの取得
        List<Map<String,Object>> detailPattern = mm059Mgr.getDetailPattern(zoneCd, series,
                generation, ctSpecCd, ppAreaCd, lineCd, ctWorkplaceCd, fromDate, tempDstnctn);
        if (detailPattern == null || detailPattern.isEmpty()) {
            //「ＥＲＲ０４７：該当データはありません。」
            throw new ApplicationException("result|error.data.not.exist");
        }
        Mmoc47BdyAttWrkCtMstrValueUtil.transferDetailPatternToValue(detailPattern, value);

        List<List<BdyAttWrkctDtlMstrEntity>> entityListArr =
                new ArrayList<List<BdyAttWrkctDtlMstrEntity>>(rowData.size());

        BdyAttWrkctDtlMstrMgr mm060Mgr = new BdyAttWrkctDtlMstrMgr(this.con);
        // 明細ループ：縦
        List<Mmoc47BdyAttWrkCtMstrListItemValue> displayList = value.getDisplayListArray();
        for (int i = 0; i < displayList.size(); i++) {
            Mmoc47BdyAttWrkCtMstrListItemValue listItemValue = displayList.get(i);

            String tmpZoneCd = listItemValue.getZoneCd();
            String tmpSeries = listItemValue.getSeries();
            String tmpGeneration = listItemValue.getGeneration();
            String tmpCtSpecCd = listItemValue.getCtSpec();
            String tmpPpAreaCd = listItemValue.getPpArea();
            String tmpSpecSupp = listItemValue.getSpecSupp();
            String tmpDestSupp = listItemValue.getDestSupp();
            String tmpApplMchnCd = listItemValue.getApplMachineCd();
            String tmpClssCd = listItemValue.getClassCd();
            String tmpLineCd = listItemValue.getLineCd();
            String tmpCtWorkplaceCd = listItemValue.getCtWorkPlace();
            Date tmpFromDate = listItemValue.getFromDate();
            Date tmpToDate = listItemValue.getToDate();
            String tmpTempDstnctn = listItemValue.getTempCategory();

            // DETAILデータの取得
            List<BdyAttWrkctDtlMstrEntity> detailData = mm060Mgr.getDetailData(tmpZoneCd, tmpSeries,
                    tmpGeneration, tmpCtSpecCd, tmpPpAreaCd, tmpApplMchnCd, tmpClssCd, tmpLineCd,
                    tmpCtWorkplaceCd, tmpFromDate);

            entityListArr.add(detailData);
        }

        // Detailパターンにより、Entity情報をListItemValueにコピ
        transferEntityToListItemValueBasedOnPattern(value, entityListArr, displayList);

        // Compute for CT計
        computeForTotalCT(displayList);

        return value;
    }

    /**
     * 全データダウンロードボタン処理
     *
     * @param value Valueのバリュー
     * @return BdyATTWrkCTMstrListValue のバリュー
     * @throws ApplicationException エラーが発生するとき
     */
    public Mmoc47BdyAttWrkCtMstrValue download(Value value) throws ApplicationException {

        Mmoc47BdyAttWrkCtMstrValue value = (Mmoc47BdyAttWrkCtMstrValue) value;
        String zoneCd = value.getZoneCd();

        String orderBy = getOrderByColumns(
                value.getSortCond1(), value.getSortOrd1(),
                value.getSortCond2(), value.getSortOrd2(),
                value.getSortCond3(), value.getSortOrd3(),
                value.getSortCond4(), value.getSortOrd4(),
                value.getSortCond5(), value.getSortOrd5());

        BdyAttWrkctMstrMgr mm059Mgr = new BdyAttWrkctMstrMgr(this.con);

        // HEADデータの取得 (page control sql)
        List<Map<String,Object>> rowData = null;
        rowData = mm059Mgr.getRowDataForDownload(zoneCd, orderBy);
        if (rowData == null || rowData.isEmpty()) {
            //「ＥＲＲ０４７：該当データはありません。」
            throw new ApplicationException("result|error.data.not.exist");
        }
        Mmoc47BdyAttWrkCtMstrValueUtil.transferRowDataToValue(rowData, value);

        // DETAILデータのラベルパターンの取得
        List<Map<String,Object>> detailPattern = mm059Mgr.getDetailPatternForDownload(zoneCd);
        if (detailPattern == null || detailPattern.isEmpty()) {
            //「ＥＲＲ０４７：該当データはありません。」
            throw new ApplicationException("result|error.data.not.exist");
        }
        Mmoc47BdyAttWrkCtMstrValueUtil.transferDetailPatternToValue(detailPattern, value);

        List<List<BdyAttWrkctDtlMstrEntity>> entityListArr =
                new ArrayList<List<BdyAttWrkctDtlMstrEntity>>(rowData.size());

        BdyAttWrkctDtlMstrMgr mm060Mgr = new BdyAttWrkctDtlMstrMgr(this.con);
        // 明細ループ：縦
        List<Mmoc47BdyAttWrkCtMstrListItemValue> displayList = value.getDisplayListArray();
        for (int i = 0; i < displayList.size(); i++) {
            Mmoc47BdyAttWrkCtMstrListItemValue listItemValue = displayList.get(i);

            String tmpZoneCd = listItemValue.getZoneCd();
            String tmpSeries = listItemValue.getSeries();
            String tmpGeneration = listItemValue.getGeneration();
            String tmpCtSpecCd = listItemValue.getCtSpec();
            String tmpPpAreaCd = listItemValue.getPpArea();
            String tmpSpecSupp = listItemValue.getSpecSupp();
            String tmpDestSupp = listItemValue.getDestSupp();
            String tmpApplMchnCd = listItemValue.getApplMachineCd();
            String tmpClssCd = listItemValue.getClassCd();
            String tmpLineCd = listItemValue.getLineCd();
            String tmpCtWorkplaceCd = listItemValue.getCtWorkPlace();
            Date tmpFromDate = listItemValue.getFromDate();
            Date tmpToDate = listItemValue.getToDate();
            String tmpTempDstnctn = listItemValue.getTempCategory();

            // DETAILデータの取得
            List<BdyAttWrkctDtlMstrEntity> detailData = mm060Mgr.getDetailData(tmpZoneCd, tmpSeries,
                    tmpGeneration, tmpCtSpecCd, tmpPpAreaCd, tmpApplMchnCd, tmpClssCd, tmpLineCd,
                    tmpCtWorkplaceCd, tmpFromDate);

            entityListArr.add(detailData);
        }

        // Detailパターンにより、Entity情報をListItemValueにコピー.
        transferEntityToListItemValueBasedOnPattern(value, entityListArr, displayList);

        // Compute for CT計
        computeForTotalCT(displayList);

        return value;
    }

    /**
     * @param displayList
     */
    private void computeForTotalCT(List<Mmoc47BdyAttWrkCtMstrListItemValue> displayList) {

        for (int i = 0; i < displayList.size(); i++) {
            Mmoc47BdyAttWrkCtMstrListItemValue listItemValue = displayList.get(i);

            List<Mmoc47DetailValue> detailValueList = listItemValue.getDetailValueList();

            // CT計の初期化
            double ctSumMinutes = 0;
            for (int j = 0; j < detailValueList.size(); j++) {
                Mmoc47DetailValue detailValue = detailValueList.get(j);
                // CT計の加算
                ctSumMinutes = ctSumMinutes + Double.parseDouble(detailValue.getCtSum());
            }
            listItemValue.setCtSum(ctSumMinutes);
        }

    }

    /**
     * Detailパターンにより、Entity情報をListItemValueにコピー.
     *
     * @param value             Value class
     * @param entityListArr     entity list
     * @param displayList       display list
     */
    private void transferEntityToListItemValueBasedOnPattern(Mmoc47BdyAttWrkCtMstrValue value,
            List<List<BdyAttWrkctDtlMstrEntity>> entityListArr,
            List<Mmoc47BdyAttWrkCtMstrListItemValue> displayList) {

        List<Mmoc47DetailPattern> detailPatternList = value.getDetailPatternList();
        for (int i = 0; i < displayList.size(); i++) {
            Mmoc47BdyAttWrkCtMstrListItemValue listItemValue = displayList.get(i);
            List<BdyAttWrkctDtlMstrEntity> entityList = entityListArr.get(i);

            // Detailパターンにより、Entity情報をListItemValueにコピー.
            Mmoc47BdyAttWrkCtMstrListItemValueUtil.transferEntityListToListItemValue(
                    listItemValue, detailPatternList, entityList);
        }

    }

    /**
     * Get order by clause.
     *
     * @param sortCond1 ソート条件１
     * @param sortOrd1 ASC/DESC1
     * @param sortCond2 ソート条件２
     * @param sortOrd2 ASC/DESC2
     * @param sortCond3 ソート条件３
     * @param sortOrd3 ASC/DESC3
     * @param sortCond4 ソート条件４
     * @param sortOrd4 ASC/DESC4
     * @param sortCond5 ソート条件５
     * @param sortOrd5 ASC/DESC5
     * @return ORDER BY 対象カラム
     */
    private String getOrderByColumns(String sortCond1, String sortOrd1,
            String sortCond2, String sortOrd2,
            String sortCond3, String sortOrd3,
            String sortCond4, String sortOrd4,
            String sortCond5, String sortOrd5) {

        String orderByStr = "";

        StringBuffer sb = new StringBuffer();

        sb.append(" ORDER BY");
        sb.append("  LINE_CD ASC");
        sb.append(" ,CT_WORKPLACE_CD ASC");

        if (!StringUtil.isBlankOrNull(sortCond1)
                || !StringUtil.isBlankOrNull(sortCond2)
                || !StringUtil.isBlankOrNull(sortCond3)
                || !StringUtil.isBlankOrNull(sortCond4)
                || !StringUtil.isBlankOrNull(sortCond5)) {
            if (!StringUtil.isBlankOrNull(sortCond1)) {
                sb.append(" ,").append(sortCond1).append(" ").append(sortOrd1);
            }

            if (!StringUtil.isBlankOrNull(sortCond2)) {
                sb.append(" ,").append(sortCond2).append(" ").append(sortOrd2);
            }

            if (!StringUtil.isBlankOrNull(sortCond3)) {
                sb.append(" ,").append(sortCond3).append(" ").append(sortOrd3);
            }

            if (!StringUtil.isBlankOrNull(sortCond4)) {
                sb.append(" ,").append(sortCond4).append(" ").append(sortOrd4);
            }

            if (!StringUtil.isBlankOrNull(sortCond5)) {
                sb.append(" ,").append(sortCond5).append(" ").append(sortOrd5);
            }

        }
        orderByStr = sb.toString();

        return orderByStr;
    }

    /**
     * 削除ボタン処理
     *
     * @param value Valueのバリュー
     * @return BdyATTWrkCTMstrListValue のバリュー
     * @throws ApplicationException エラーが発生するとき
     */
    public Mmoc47BdyAttWrkCtMstrValue delete(Value value) throws ApplicationException {

        Mmoc47BdyAttWrkCtMstrValue value = (Mmoc47BdyAttWrkCtMstrValue) value;

        BdyAttWrkctMstrMgr mm059Mgr = new BdyAttWrkctMstrMgr(this.con);
        BdyAttWrkctDtlMstrMgr mm060Mgr = new BdyAttWrkctDtlMstrMgr(this.con);

        List<Mmoc47BdyAttWrkCtMstrListItemValue> detailList = value.getDisplayListArray();
        for (int i = 0; i < detailList.size(); i++) {
            Mmoc47BdyAttWrkCtMstrListItemValue itemValue = (Mmoc47BdyAttWrkCtMstrListItemValue) detailList.get(i);

            if (!itemValue.isChecked()) {
                continue;
            }

            BdyAttWrkctMstrEntity mm059entity = new BdyAttWrkctMstrEntity();
            Mmoc47BdyAttWrkCtMstrListItemValueUtil.transferListItemValueToEntity(
                value, itemValue, mm059entity);

            try {
                mm059Mgr.delete(value.getUserId(), mm059entity);
            } catch (EntityNotFoundException e) {
                //エラー発生せず、次の処理へ進む
            }

            java.sql.Date fromDate = null;
            if (itemValue.getFromDate() != null) {
                fromDate = new java.sql.Date(itemValue.getFromDate().getTime());
            }
            mm060Mgr.deleteBy(value.getZoneCd(),
                    itemValue.getSeries(), itemValue.getGeneration(), itemValue.getCtSpec(),
                    itemValue.getPpArea(), itemValue.getApplMachineCd(), itemValue.getClassCd(),
                    itemValue.getLineCd(), itemValue.getCtWorkPlace(), fromDate);
        }

        return value;
    }

}
