import java.sql.Date;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import jp.co.ulsystems.umlaut.dao.ResultSetReader;
import jp.co.ulsystems.umlaut.dao.sqlutil.ResultSetProxy;
import jp.co.ulsystems.umlaut.dao.sqlutil.SQLAndCondition;
import jp.co.ulsystems.umlaut.dao.sqlutil.SQLExpression;
import jp.co.ulsystems.umlaut.dao.sqlutil.SQLExpressionConst;

public final class BdyAttWrkctMstrMgr {

    /**
     * BdyAttWrkctMstrMgrを作成する。
     *
     * @param con java.sql.Connection コネクション
     */
    public BdyAttWrkctMstrMgr(java.sql.Connection con) {
        super(BdyAttWrkctMstrPersister.getInstance(), con);
    }

    /**
     * Get content for Line Code dropdown.
     *
     * @param zoneCd        事業拠点コード
     * @param localeLangCd  言語コード
     * @return Content for Line Code drop down
     */
    public List<Map<String,Object>> queryLineCd(String zoneCd, String localeLangCd) {

        StringBuffer sqlSb = new StringBuffer();

        sqlSb.append("  SELECT ");
        sqlSb.append("      LINE_CD ");
        sqlSb.append("  FROM ");
        sqlSb.append("      OPPL_LINECD_MSTR MM032 ");
        sqlSb.append("  WHERE ");
        sqlSb.append("      MM032.ZONE_CD = ? ");
        sqlSb.append("      AND MM032.LANG_CD = ? ");
        sqlSb.append("      AND MM032.WP_CT_RGST_DSTNCTN IN ('1', '2') ");
        sqlSb.append("  ORDER BY ");
        sqlSb.append("      MM032.LINE_CD ");

        List<String> preValues = new ArrayList<String>();
        preValues.add(zoneCd);
        preValues.add(localeLangCd);

        // 検索を実行し、結果をＨａｓｈＭａｐの配列として返す
        final List<Map<String,Object>> result = new ArrayList<Map<String,Object>>();
        super.execFreeQuery(sqlSb.toString(), preValues, new ResultSetReader() {
            public void set(ResultSet rs) throws SQLException {
                while (rs.next()) {
                    Map<String,Object> record = new HashMap<String,Object>();
                    ResultSetMetaData rsmd = rs.getMetaData();
                    record.put(rsmd.getColumnName(1), rs.getString(1));
                    result.add(record);
                }
            }
        });

        return result;

    }

    /**
     * 検索用対象データを取得
     *
     * @param zoneCd        事業拠点コード
     * @param series        シリーズ
     * @param generation    世代
     * @param ctSpecCd      CT登録用仕様コード
     * @param ppAreaCd      生産計画仕向地コード
     * @param lineCd        ラインコード
     * @param ctWorkplaceCd CT登録職場コード
     * @param fromDate      有効日
     * @param tempDstnctn   正仮区分
     * @param orderBy       ORDER BY句
     * @param startRow      開始行
     * @param endRow        終了行
     * @return  対象データ一覧
     */
    public List<Map<String, Object>> getRowData(String zoneCd, String series, String generation,
            String ctSpecCd, String ppAreaCd, String lineCd, String ctWorkplaceCd,
            java.util.Date fromDate, String tempDstnctn, String orderBy, int startRow, int endRow) {

        SQLAndCondition cond = getConditionForMMOC47(zoneCd, series, generation, ctSpecCd,
                ppAreaCd, lineCd, ctWorkplaceCd, fromDate, tempDstnctn);

        /*
         * SQL requires three levels of SQL subqueries:
         * 1st (top-most): subquery with condition for paging (rownum)
         * 2nd (mid): subquery with ROWNUM
         * 3rd (inner-most): sub-query with the search conditions and "ORDER BY"
         *   (ROWNUM is not included because it gets distorted by "ORDER BY")
         */
        StringBuffer sqlSb = new StringBuffer();
        sqlSb.append("  SELECT * FROM ");
        sqlSb.append("  ( ");
        sqlSb.append("  SELECT ROWNUM AS RNUM, SUB.* ");
        sqlSb.append("  FROM ");
        sqlSb.append("      ( ");
        sqlSb.append("          SELECT ");
        sqlSb.append("              MM059.* ");
        sqlSb.append("          FROM ");
        sqlSb.append("              BDY_ATT_WRKCT_MSTR MM059 ");
        sqlSb.append(           cond.getStringWithWhere(true) + "\n");
        sqlSb.append(orderBy);
        sqlSb.append("      ) SUB ");
        sqlSb.append("  ) ");

        // ページング用条件を追加
        SQLAndCondition pagingCond = new SQLAndCondition();
        pagingCond.add(new SQLExpression("", "RNUM", GE, startRow));
        pagingCond.add(new SQLExpression("", "RNUM", LE, endRow));
        sqlSb.append(pagingCond.getStringWithWhere(true) + "\n");

        // 検索を実行し、結果をＨａｓｈＭａｐの配列として返す
        final List<Map<String,Object>> result = new ArrayList<Map<String,Object>>();
        super.execFreeQuery(sqlSb.toString(), new ResultSetReader() {
            public void set(ResultSet rs) throws SQLException {

                ResultSetMetaData rsmd = rs.getMetaData();
                int colCount = rsmd.getColumnCount();

                while (rs.next()) {
                    Map<String,Object> record = new HashMap<String,Object>();

                    for (int colIndex = 1; colIndex <= colCount; colIndex++) {
                        Object data = rs.getObject(colIndex);
                        //set data type to timestamp for date types
                        if (data instanceof Date || data instanceof Timestamp) {
                            data = rs.getTimestamp(colIndex);
                        }

                        record.put(rsmd.getColumnName(colIndex), data);
                    }

                    result.add(record);
                }

            }
        });

        return result;
    }

    /**
     * (MMOC47の検索条件を取得
     *
     * @param zoneCd        事業拠点コード
     * @param series        シリーズ
     * @param generation    世代
     * @param ctSpecCd      CT登録用仕様コード
     * @param ppAreaCd      生産計画仕向地コード
     * @param lineCd        ラインコード
     * @param ctWorkplaceCd CT登録職場コード
     * @param fromDate      有効日
     * @param tempDstnctn   正仮区分
     * @param orderBy       ORDER BY句
     * @param startRow      開始行
     * @param endRow        終了行
     * @return  検索条件
     */
    private SQLAndCondition getConditionForMMOC47(String zoneCd, String series, String generation, String ctSpecCd,
            String ppAreaCd, String lineCd, String ctWorkplaceCd, java.util.Date fromDate, String tempDstnctn) {
        SQLAndCondition cond = new SQLAndCondition();

        if (!StringUtil.isBlankOrNull(zoneCd)) {
            cond.add(new SQLExpression("MM059", "ZONE_CD", SQLExpressionConst.EQ, zoneCd));
        }
        if (!StringUtil.isBlankOrNull(series)) {
            cond.add(new SQLExpression("MM059", "SERIES", SQLExpressionConst.EQ, series));
        }
        if (!StringUtil.isBlankOrNull(generation)) {
            cond.add(new SQLExpression("MM059", "GENERATION", SQLExpressionConst.EQ, generation));
        }
        if (!StringUtil.isBlankOrNull(ctSpecCd)) {
            cond.add(new SQLExpression("MM059", "CT_SPEC_CD", SQLExpressionConst.EQ, ctSpecCd));
        }
        if (!StringUtil.isBlankOrNull(ppAreaCd)) {
            cond.add(new SQLExpression("MM059", "PP_AREA_CD", SQLExpressionConst.EQ, ppAreaCd));
        }
        if (!StringUtil.isBlankOrNull(lineCd)) {
            cond.add(new SQLExpression("MM059", "LINE_CD", SQLExpressionConst.EQ, lineCd));
        }
        if (!StringUtil.isBlankOrNull(ctWorkplaceCd)) {
            cond.add(new SQLExpression("MM059", "CT_WORKPLACE_CD", SQLExpressionConst.EQ,
                    ctWorkplaceCd));
        }
        if (fromDate != null) {
            cond.add(new SQLExpression("MM059", "FROM_DATE", SQLExpressionConst.LE, fromDate));
            cond.add(new SQLExpression("MM059", "TO_DATE", SQLExpressionConst.GT, fromDate));
        }
        if (!StringUtil.isBlankOrNull(tempDstnctn) && !tempDstnctn.equals("*")) {
            cond.add(new SQLExpression("MM059", "TEMP_DSTNCTN", SQLExpressionConst.EQ,
                tempDstnctn));
        }
        return cond;
    }

    /**
     * 検索用DETAILデータのラベルパターンを取得
     *
     * @param zoneCd        事業拠点コード
     * @param series        シリーズ
     * @param generation    世代
     * @param ctSpecCd      CT登録用仕様コード
     * @param ppAreaCd      生産計画仕向地コード
     * @param lineCd        ラインコード
     * @param ctWorkplaceCd CT登録職場コード
     * @param fromDate      有効日
     * @param tempDstnctn   正仮区分
     * @return DETAILデータのラベルパターン
     */
    public List<Map<String, Object>> getDetailPattern(String zoneCd, String series, String generation,
            String ctSpecCd, String ppAreaCd, String lineCd, String ctWorkplaceCd,
            java.util.Date fromDate, String tempDstnctn) {
        SQLAndCondition cond = getConditionForMMOC47(zoneCd, series, generation, ctSpecCd,
                ppAreaCd, lineCd, ctWorkplaceCd, fromDate, tempDstnctn);

        // JOIN MM059 and MM060
        cond.add(new SQLExpression("MM059", "ZONE_CD", SQLExpressionConst.EQ,
                "MM060", "ZONE_CD"));
        cond.add(new SQLExpression("MM059", "SERIES", SQLExpressionConst.EQ,
                "MM060", "SERIES"));
        cond.add(new SQLExpression("MM059", "GENERATION", SQLExpressionConst.EQ,
                "MM060", "GENERATION"));
        cond.add(new SQLExpression("MM059", "CT_SPEC_CD", SQLExpressionConst.EQ,
                "MM060", "CT_SPEC_CD"));
        cond.add(new SQLExpression("MM059", "PP_AREA_CD", SQLExpressionConst.EQ,
                "MM060", "PP_AREA_CD"));
        cond.add(new SQLExpression("MM059", "APPL_MCHN_CD", SQLExpressionConst.EQ,
                "MM060", "APPL_MCHN_CD"));
        cond.add(new SQLExpression("MM059", "CLSS_CD", SQLExpressionConst.EQ,
                "MM060", "CLSS_CD"));
        cond.add(new SQLExpression("MM059", "LINE_CD", SQLExpressionConst.EQ,
                "MM060", "LINE_CD"));
        cond.add(new SQLExpression("MM059", "CT_WORKPLACE_CD", SQLExpressionConst.EQ,
                "MM060", "CT_WORKPLACE_CD"));
        cond.add(new SQLExpression("MM059", "FROM_DATE", SQLExpressionConst.EQ,
                "MM060", "FROM_DATE"));

        StringBuffer sqlSb = new StringBuffer();
        sqlSb.append("  SELECT ");
        sqlSb.append("      MM060.LINE_CD ");
        sqlSb.append("      ,MM060.CT_WORKPLACE_CD ");
        sqlSb.append("      ,MM060.WORKPLACE_CD ");
        sqlSb.append("      ,MM060.OPERATION_NO ");
        sqlSb.append("      ,MM060.OPE_CONTENTS ");
        sqlSb.append("  FROM ");
        sqlSb.append("      BDY_ATT_WRKCT_MSTR MM059 ");
        sqlSb.append("      ,BDY_ATT_WRKCT_DTL_MSTR MM060 ");
        sqlSb.append(cond.getStringWithWhere(true) + "\n");
        sqlSb.append("  GROUP BY ");
        sqlSb.append("      MM060.LINE_CD ");
        sqlSb.append("      ,MM060.CT_WORKPLACE_CD ");
        sqlSb.append("      ,MM060.WORKPLACE_CD ");
        sqlSb.append("      ,MM060.OPERATION_NO ");
        sqlSb.append("      ,MM060.OPE_CONTENTS ");
        sqlSb.append("  ORDER BY ");
        sqlSb.append("      MM060.LINE_CD ");
        sqlSb.append("      ,MM060.CT_WORKPLACE_CD ");
        sqlSb.append("      ,MM060.WORKPLACE_CD ");
        sqlSb.append("      ,MM060.OPERATION_NO ");
        sqlSb.append("      ,MM060.OPE_CONTENTS ");

        // 検索を実行し、結果をＨａｓｈＭａｐの配列として返す
        final List<Map<String,Object>> result = new ArrayList<Map<String,Object>>();
        super.execFreeQuery(sqlSb.toString(), new ResultSetReader() {
            public void set(ResultSet rs) throws SQLException {

                ResultSetMetaData rsmd = rs.getMetaData();
                int colCount = rsmd.getColumnCount();

                while (rs.next()) {
                    Map<String,Object> record = new HashMap<String,Object>();

                    for (int colIndex = 1; colIndex <= colCount; colIndex++) {
                        Object data = rs.getObject(colIndex);
                        //set data type to timestamp for date types
                        if (data instanceof Date || data instanceof Timestamp) {
                            data = rs.getTimestamp(colIndex);
                        }

                        record.put(rsmd.getColumnName(colIndex), data);
                    }

                    result.add(record);
                }

            }
        });

        return result;
    }

    /**
     * 検索用該当データの件数を取得する。
     *
     * @param zoneCd        事業拠点コード
     * @param series        シリーズ
     * @param generation    世代
     * @param ctSpecCd      CT登録用仕様コード
     * @param ppAreaCd      生産計画仕向地コード
     * @param lineCd        ラインコード
     * @param ctWorkplaceCd CT登録職場コード
     * @param fromDate      有効日
     * @param tempDstnctn   正仮区分
     * @return  該当データの件数
     */
    public int countRowData(String zoneCd, String series, String generation, String ctSpecCd,
            String ppAreaCd, String lineCd, String ctWorkplaceCd, java.util.Date fromDate,
            String tempDstnctn) {

        // COUNTのSQLを作成する
        SQLAndCondition cond = getConditionForMMOC47(zoneCd, series, generation, ctSpecCd,
                ppAreaCd, lineCd, ctWorkplaceCd, fromDate, tempDstnctn);

        StringBuffer sqlSb = new StringBuffer();
        sqlSb.append("  SELECT ");
        sqlSb.append("      COUNT(*) ");
        sqlSb.append("  FROM ");
        sqlSb.append("      BDY_ATT_WRKCT_MSTR MM059 ");
        sqlSb.append(cond.getStringWithWhere(true) + "\n");

        // ＳＱＬを実行する
        final List<String> resultList = new ArrayList<String>();
        super.execFreeQuery(sqlSb.toString(), new ResultSetReader() {
            public void set(ResultSet rs) throws SQLException {
                if (rs.next()) {
                    // 件数
                    resultList.add(rs.getString(1));
                }
            }
        });

        // 検索件数を返す
        return ConverterUtil.toInt((String) resultList.get(0));
    }

    /**
     * 全データダウンロード用対象データを取得
     *
     * @param zoneCd        事業拠点コード
     * @param orderBy       ORDER BY句
     * @return 全ダウンロード対象データ一覧
     */
    public List<Map<String, Object>> getRowDataForDownload(String zoneCd, String orderBy) {
        SQLAndCondition cond = new SQLAndCondition();
        if (!StringUtil.isBlankOrNull(zoneCd)) {
            cond.add(new SQLExpression("MM059", "ZONE_CD", SQLExpressionConst.EQ, zoneCd));
        }

        StringBuffer sqlSb = new StringBuffer();
        sqlSb.append("  SELECT ");
        sqlSb.append("      MM059.* ");
        sqlSb.append("  FROM ");
        sqlSb.append("      BDY_ATT_WRKCT_MSTR MM059 ");
        sqlSb.append(           cond.getStringWithWhere(true) + "\n");
        sqlSb.append(orderBy);

        // 検索を実行し、結果をＨａｓｈＭａｐの配列として返す
        final List<Map<String,Object>> result = new ArrayList<Map<String,Object>>();
        super.execFreeQuery(sqlSb.toString(), new ResultSetReader() {
            public void set(ResultSet rs) throws SQLException {

                ResultSetMetaData rsmd = rs.getMetaData();
                int colCount = rsmd.getColumnCount();

                while (rs.next()) {
                    Map<String,Object> record = new HashMap<String,Object>();

                    for (int colIndex = 1; colIndex <= colCount; colIndex++) {
                        Object data = rs.getObject(colIndex);
                        //set data type to timestamp for date types
                        if (data instanceof Date || data instanceof Timestamp) {
                            data = rs.getTimestamp(colIndex);
                        }

                        record.put(rsmd.getColumnName(colIndex), data);
                    }

                    result.add(record);
                }

            }
        });

        return result;
    }

    /**
     * 全データダウンロード用DETAILデータのラベルパターンを取得。
     *
     * @param zoneCd        事業拠点コード
     * @return DETAILデータのラベルパターン
     */
    public List<Map<String, Object>> getDetailPatternForDownload(String zoneCd) {

        SQLAndCondition cond = new SQLAndCondition();
        cond.add(new SQLExpression("MM059", "ZONE_CD", SQLExpressionConst.EQ, zoneCd));

        // JOIN MM059 and MM060
        cond.add(new SQLExpression("MM059", "ZONE_CD", SQLExpressionConst.EQ,
                "MM060", "ZONE_CD"));
        cond.add(new SQLExpression("MM059", "SERIES", SQLExpressionConst.EQ,
                "MM060", "SERIES"));
        cond.add(new SQLExpression("MM059", "GENERATION", SQLExpressionConst.EQ,
                "MM060", "GENERATION"));
        cond.add(new SQLExpression("MM059", "CT_SPEC_CD", SQLExpressionConst.EQ,
                "MM060", "CT_SPEC_CD"));
        cond.add(new SQLExpression("MM059", "PP_AREA_CD", SQLExpressionConst.EQ,
                "MM060", "PP_AREA_CD"));
        cond.add(new SQLExpression("MM059", "APPL_MCHN_CD", SQLExpressionConst.EQ,
                "MM060", "APPL_MCHN_CD"));
        cond.add(new SQLExpression("MM059", "CLSS_CD", SQLExpressionConst.EQ,
                "MM060", "CLSS_CD"));
        cond.add(new SQLExpression("MM059", "LINE_CD", SQLExpressionConst.EQ,
                "MM060", "LINE_CD"));
        cond.add(new SQLExpression("MM059", "CT_WORKPLACE_CD", SQLExpressionConst.EQ,
                "MM060", "CT_WORKPLACE_CD"));
        cond.add(new SQLExpression("MM059", "FROM_DATE", SQLExpressionConst.EQ,
                "MM060", "FROM_DATE"));

        StringBuffer sqlSb = new StringBuffer();
        sqlSb.append("  SELECT ");
        sqlSb.append("      MM060.LINE_CD ");
        sqlSb.append("      ,MM060.CT_WORKPLACE_CD ");
        sqlSb.append("      ,MM060.WORKPLACE_CD ");
        sqlSb.append("      ,MM060.OPERATION_NO ");
        sqlSb.append("      ,MM060.OPE_CONTENTS ");
        sqlSb.append("  FROM ");
        sqlSb.append("      BDY_ATT_WRKCT_MSTR MM059 ");
        sqlSb.append("      ,BDY_ATT_WRKCT_DTL_MSTR MM060 ");
        sqlSb.append(cond.getStringWithWhere(true) + "\n");
        sqlSb.append("  GROUP BY ");
        sqlSb.append("      MM060.LINE_CD ");
        sqlSb.append("      ,MM060.CT_WORKPLACE_CD ");
        sqlSb.append("      ,MM060.WORKPLACE_CD ");
        sqlSb.append("      ,MM060.OPERATION_NO ");
        sqlSb.append("      ,MM060.OPE_CONTENTS ");
        sqlSb.append("  ORDER BY ");
        sqlSb.append("      MM060.LINE_CD ");
        sqlSb.append("      ,MM060.CT_WORKPLACE_CD ");
        sqlSb.append("      ,MM060.WORKPLACE_CD ");
        sqlSb.append("      ,MM060.OPERATION_NO ");
        sqlSb.append("      ,MM060.OPE_CONTENTS ");

        // 検索を実行し、結果をＨａｓｈＭａｐの配列として返す
        final List<Map<String,Object>> result = new ArrayList<Map<String,Object>>();
        super.execFreeQuery(sqlSb.toString(), new ResultSetReader() {
            public void set(ResultSet rs) throws SQLException {

                ResultSetMetaData rsmd = rs.getMetaData();
                int colCount = rsmd.getColumnCount();

                while (rs.next()) {
                    Map<String,Object> record = new HashMap<String,Object>();

                    for (int colIndex = 1; colIndex <= colCount; colIndex++) {
                        Object data = rs.getObject(colIndex);
                        //set data type to timestamp for date types
                        if (data instanceof Date || data instanceof Timestamp) {
                            data = rs.getTimestamp(colIndex);
                        }

                        record.put(rsmd.getColumnName(colIndex), data);
                    }

                    result.add(record);
                }

            }
        });

        return result;
    }

}
