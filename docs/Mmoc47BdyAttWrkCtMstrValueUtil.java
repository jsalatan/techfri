import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.apache.struts.action.ActionForm;

public class Mmoc47BdyAttWrkCtMstrValueUtil {

    private static final int MAX_OPE_CONTENT_LENGTH = 30;

    /** Prevent this class to be instantiated */
    private Mmoc47BdyAttWrkCtMstrValueUtil() {
    }

    /**
     * FormからValueへデータをコピー。
     *
     * @param form            データ元フォームクラス
     * @param value           データコピー先バリュークラス
     */
    public static void transferFormToValue(ActionForm actionForm,
            MmSearchValue mmValue) {

        // ValueUtilHelperで文字列の内部メンバーを一括コピー
        ValueUtilHelper.transferFormToValue(actionForm, mmValue);

        // フォーム⇒バリューのメンバータイプが異なる場合、個別でコピー
        Mmoc47BdyAttWrkCtMstrForm form = (Mmoc47BdyAttWrkCtMstrForm) actionForm;
        Mmoc47BdyAttWrkCtMstrValue value = (Mmoc47BdyAttWrkCtMstrValue) mmValue;
        try {
            String dateStr = form.getFromDate();
            if (!StringUtil.isBlankOrNull(dateStr)) {
                SimpleDateFormat sdf = new SimpleDateFormat("yyyy/MM/dd");
                value.setFromDate(sdf.parse(dateStr));
            }
        } catch (ParseException e) {
            logger.info(e);
        }

        Mmoc47BdyAttWrkCtMstrListItemForm[] detailList = form.getDetailList();
        List<Mmoc47BdyAttWrkCtMstrListItemValue> itemValList =
                new ArrayList<Mmoc47BdyAttWrkCtMstrListItemValue>();
        for (int i = 0; i < detailList.length; i++) {
            Mmoc47BdyAttWrkCtMstrListItemForm lstItmForm = detailList[i];
            Mmoc47BdyAttWrkCtMstrListItemValue lstItmVal =
                    new Mmoc47BdyAttWrkCtMstrListItemValue();
            Mmoc47BdyAttWrkCtMstrListItemValueUtil.transferFormToValue(lstItmForm, lstItmVal);

            itemValList.add(lstItmVal);
        }
        value.setDisplayListArray(itemValList);

    }

    /**
     * ValueからFormへデータを詰め替えます。
     *
     * @param value           データ元バリュークラス
     * @param form            データコピー先フォームクラス
     */
    public static void transferValueToForm(Mmoc47BdyAttWrkCtMstrValue value,
            Mmoc47BdyAttWrkCtMstrForm form) {

        // ValueUtilHelperで文字列の内部メンバーを一括コピー
        ValueUtilHelper.transferValueToForm(value, form);

        // バリュー⇒フォームのメンバータイプが異なる場合、個別でコピー
        List<Mmoc47BdyAttWrkCtMstrListItemValue> listItmValArray = value.getDisplayListArray();
        Mmoc47BdyAttWrkCtMstrListItemForm[] formArr =
                new Mmoc47BdyAttWrkCtMstrListItemForm[listItmValArray.size()];
        for (int i = 0; i < listItmValArray.size(); i++) {
            Mmoc47BdyAttWrkCtMstrListItemValue listItmVal = listItmValArray.get(i);
            Mmoc47BdyAttWrkCtMstrListItemForm lstItmForm =
                    new Mmoc47BdyAttWrkCtMstrListItemForm();
            Mmoc47BdyAttWrkCtMstrListItemValueUtil.transferValueToForm(listItmVal, lstItmForm);

            formArr[i] = lstItmForm;
        }
        form.setDetailList(formArr);

        // DETAILデータのラベルパターンをコピー
        List<Mmoc47DetailPattern> detailPatternList = value.getDetailPatternList();
        for (Mmoc47DetailPattern detailPattern : detailPatternList) {
            String opeContents = detailPattern.getOpeContents();
            if (opeContents != null && opeContents.length() > MAX_OPE_CONTENT_LENGTH) {
                opeContents = opeContents.substring(0, MAX_OPE_CONTENT_LENGTH);
            }
            detailPattern.setOpeContents(opeContents);
        }
        Mmoc47DetailPattern[] patternArr = new Mmoc47DetailPattern[detailPatternList.size()];
        detailPatternList.toArray(patternArr);


        form.setDetailPatternList(patternArr);

    }

    /**
     * Ｍａｐになっている検索結果一覧をListItemValueの一覧に変換し、渡したvalueインスタンスに設定する。
     *
     * @param result 検索結果のMap一覧
     * @param value コピー先のバリューオブジェクト
     */
    public static void transferRowDataToValue(List<Map<String, Object>> result,
            Mmoc47BdyAttWrkCtMstrValue value) {

        // ListItemValueの配列を初期化
        List<Mmoc47BdyAttWrkCtMstrListItemValue> displayList =
                new ArrayList<Mmoc47BdyAttWrkCtMstrListItemValue>();

        // 各エンティティマップをListItemValueに変換(値をコピー)
        for (Map<String, Object> entityMap : result) {
            Mmoc47BdyAttWrkCtMstrListItemValue lstItmVal =
                    new Mmoc47BdyAttWrkCtMstrListItemValue();
            Mmoc47BdyAttWrkCtMstrListItemValueUtil.transferRowDataToListItemValue(entityMap,
                    lstItmVal);
            displayList.add(lstItmVal);
        }

        // バリュークラスに設定
        value.setDisplayListArray(displayList);

    }

    /**
     * DETAILデータのラベルパターンマップをValueクラスに格納.
     *
     * @param detailPatternMap  DETAILデータのラベルパターンマップ
     * @param value             格納先Value
     */
    public static void transferDetailPatternToValue(List<Map<String, Object>> detailPatternMap,
            Mmoc47BdyAttWrkCtMstrValue value) {

        // ListItemValueの配列を初期化
        List<Mmoc47DetailPattern> detailPatternList = new ArrayList<Mmoc47DetailPattern>();

        // 各エンティティマップをDetailPatternに変換(値をコピー)
        for (Map<String, Object> entityMap : detailPatternMap) {
            Mmoc47DetailPattern detailPattern = new Mmoc47DetailPattern();
            Mmoc47BdyAttWrkCtMstrListItemValueUtil.transferDataMapToHeaderLabel(entityMap,
                    detailPattern);
            detailPatternList.add(detailPattern);
        }

        // バリュークラスに設定
        value.setDetailPatternList(detailPatternList);

    }
}
