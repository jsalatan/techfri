<%@ page language="java" contentType="text/html; charset=UTF-8"
  pageEncoding="Shift_JIS"%>

<%@ include file="/include/header_top2.jsp"%>
<title>MMOC47 <bean:message key="label.title.mmoc47" /></title>
<script type="text/javascript" src="<%=request.getContextPath()%>/js/jquery-1.11.3.min.js" ></script>

<script type="text/javascript">
  function submitProcess(arg) {
    if (_checkDuplicatedSubmit()) {
      document.forms["mmoc47BdyAttWrkCtMstrForm"].process.value = arg;
      document.forms["mmoc47BdyAttWrkCtMstrForm"].submit();
    }
  }

  //連打防止
  var _isClicked = false;
  function _checkDuplicatedSubmit() {
    if (_isClicked) {
      var errMsg = '<bean:message key="info.double.submit.prohibited"/>';
      //if (!confirm("一度ボタンが押されており再押下できません。キャンセルを選ぶことで押下禁止を解除できます。")) _isClicked = false;
      if (!confirm(errMsg))
        _isClicked = false;
      return false;
    } else {
      //alert("submitted: " + event.srcElement.type);
      _isClicked = true;
      return true;
    }
  }

  /**
   * チェックＢＯＸの判定処理
   *　(true or false)
   */
  function tickAll() {
    var inputs = document.getElementsByTagName("input");
    var tickVal = document.forms[0].elements["checkAll"].checked;
    for(var i = 0; i < inputs.length; i++) {
        if(inputs[i].type == "checkbox" && inputs[i].name != "checkAll") {
            inputs[i].checked = tickVal;
        }
    }
  }

  function showHide(thisBtn) {

    var visibility = "";
    if (thisBtn.value == '<<') {
      visibility = "none";
      thisBtn.value = ">>";
    } else {
      visibility = "";
      thisBtn.value = "<<";
    }

    $('.to-hide').css('display', visibility);
  }

  function closeWin() {
    window.close();
  }

  function noenter() {
    if (window.event.keyCode == 13) {
      window.event.keyCode = 0;
    }
  }

  function confirmDelete() {
    if (confirm('<bean:message key="label.inf.delete" />')) {
        submitProcess('delete');
    }
  }

  function register() {
    location.href='<%=request.getContextPath()%>/mm_bdy_att_wrkct_dtl_mstr_search.do';
  }

  function copyRegister() {
    // 1. check if at least one is checked
    if (!checkOnlyOne()) {
      return false;
    }

    // 2. get checked index
    var index = getCheckedIndex();

    // 3. get hidden values to pass as parameter
    var urlParams = getUrlParameters(index);
    location.href='<%=request.getContextPath()%>'
      + '/mm_bdy_att_wrkct_dtl_mstr_search.do?copyModeFlag=1&' + urlParams;

  }

  function edit(param) {
    // 1. check if at least one is checked
    if (!checkOnlyOne()) {
      return false;
    }

    // 2. get checked index
    var index = getCheckedIndex();

    // 3. get hidden values to pass as parameter
    var urlParams = getUrlParameters(index);
    location.href='<%=request.getContextPath()%>'
      + '/mm_bdy_att_wrkct_dtl_mstr_search.do?' + urlParams;

  }

  function getCheckedIndex() {

    var index = -1;
    $(':checkbox:not(input[name="checkAll"])').each(function(i) {
      if (this.checked) {
        index = i;
        return false;
      }
    });

    return index;

  }

  function checkOnlyOne() {
    var checkCount = $(':checkbox:not(input[name="checkAll"]):checked').length;

    var checkOneOnly = false;
    if (checkCount != 1) {
      document.getElementById("atleastone").style.display = "inline";
    } else {
      document.getElementById("atleastone").style.display = "none";
      checkOneOnly = true;
    }

    return checkOneOnly;
  }


  function getUrlParameters(index) {
    var series = document.getElementsByName("detailList[" + index + "].series")[0].value;
    var generation = document.getElementsByName("detailList[" + index + "].generation")[0].value;
    var ctSpec = document.getElementsByName("detailList[" + index + "].ctSpec")[0].value;
    var ppArea = document.getElementsByName("detailList[" + index + "].ppArea")[0].value;
    var applMachineCd = document.getElementsByName("detailList[" + index + "].applMachineCd")[0].value;
    var classCd = document.getElementsByName("detailList[" + index + "].classCd")[0].value;
    var lineCd = document.getElementsByName("detailList[" + index + "].lineCd")[0].value;
    var ctWorkPlace = document.getElementsByName("detailList[" + index + "].ctWorkPlace")[0].value;
    var fromDate = document.getElementsByName("detailList[" + index + "].fromDate")[0].value;

    var urlParams = 'series=' + series
      + '&generation=' + generation
      + '&ctSpecCd=' + ctSpec
      + '&ppAreaCd=' + ppArea
      + '&applMachineCd=' + applMachineCd
      + '&classCd=' + classCd
      + '&lineCd=' + lineCd
      + '&ctWorkPlaceCd=' + ctWorkPlace
      + '&fromDate=' + fromDate;

    return urlParams;
  }

</script>

<html:form action="/mmoc47_bdy_att_wrkct_mstr_transfer.do">

    <!-- １段目 システム共通ヘッダー -->
    <%@ include file="/include/header_bottom.jsp"%>
    <!-- ３段目 画面名 -->
    <table class="TITLE">
      <tr>
        <td class="TITLE"><html:link
            page="/mmoc47_bdy_att_wrkct_mstr_view.do">
            <bean:message key="label.title.mmoc47" />
          </html:link></td>
      </tr>
    </table>

    <!-- 画面設計領域 -->
    <table border class="FRAME">
      <tr>
        <td class="FRAME">
          <table class="MESSAGE">
            <tr>
              <td class="MESSAGE">
                <html:errors property="error.top" />
                <html:errors property="result" />
                <span id="atleastone" style="display:none;" ><bean:message key="error.select.atleast.one" /></span>
                <font color="blue"><b><bean:write name="mmoc47BdyAttWrkCtMstrForm" property="message" /></b></font>
              </td>
            </tr>
          </table>

          <table width="100%">
            <tbody>
              <tr>
                <td>
                  <input type="button" value='<bean:message key="label.search" />' class="BUTTON" onclick="submitProcess('search')"/>
                  <bean:message key="label.mmoc47.row.count" />
                  <html:text name="mmoc47BdyAttWrkCtMstrForm" property="pageSize" size="3"
                      maxlength="3" style="text-align:right" onkeypress="return noenter()"/>&nbsp;&nbsp;
                  <html:errors property="pageSize" />
                  <bean:message key="label.mmoc47.column.count" />
                  <html:text name="mmoc47BdyAttWrkCtMstrForm" property="colSize" size="3"
                      maxlength="3" style="text-align:right" onkeypress="return noenter()"/>&nbsp;&nbsp;
                  <html:errors property="colSize" />

                  <input type="button" value='<bean:message key="label.register" />' class="BUTTON" onclick="register()"/>

                  <bean:size id="sizeForSearch" name="mmoc47BdyAttWrkCtMstrForm" property="detailList"/>
                  <logic:greaterThan name="sizeForSearch" value="0" >
                    <input type="button" value='<bean:message key="label.copy_register" />' class="BUTTON" onclick="copyRegister()"/>
                    <input type="button" value='<bean:message key="label.update" />' class="BUTTON" onclick="edit()"/>
                  </logic:greaterThan>
                  <logic:lessThan name="sizeForSearch" value="1" >
                    <input type="button" value='<bean:message key="label.copy_register" />' class="BUTTON" disabled="disabled"/>
                    <input type="button" value='<bean:message key="label.update" />' class="BUTTON" disabled="disabled"/>
                  </logic:lessThan>

                  <span style="width:50px;  height:auto; display:inline-block;">&nbsp;</span>
                  <input type="button" value='<bean:message key="label.mmoc47.download_all_data" />' class="BUTTON" onclick="submitProcess('download')"/>
                  <span style="width:50px;  height:auto; display:inline-block;">&nbsp;</span>
                  <logic:functionPermission functionName="prdct_asst_sct_chief">
                    <logic:greaterThan name="sizeForSearch" value="0" >
                      <input type="button" value='<bean:message key="label.delete" />' class="BUTTON" onclick="confirmDelete()"/>
                    </logic:greaterThan>
                    <logic:lessThan name="sizeForSearch" value="1" >
                      <input type="button" value='<bean:message key="label.delete" />' class="BUTTON" disabled="disabled"/>
                    </logic:lessThan>
                  </logic:functionPermission>
                  <logic:notFunctionPermission functionName="prdct_asst_sct_chief">
                    <input type="button" value='<bean:message key="label.delete" />' class="BUTTON" disabled="disabled"/>
                  </logic:notFunctionPermission>
                </td>
              </tr>
            </tbody>
          </table>
          <table class="SEARCH">
          <!--
            TODO: to retain dropdown contents without re-search from DB:
              http://www.javalobby.org/articles/struts/?source=archives
           -->
           <tbody>
              <tr>
                <th class="SEARCH"><!-- ラインコード --><bean:message key="label.mmoc47.line_cd" /></th>
                <td class="SEARCH_MUST_INPUT" nowrap>
                    <bean:define id="dropLineCd" name="mmoc47BdyAttWrkCtMstrForm" property="dropLineCd" type="java.util.List" />
                    <html:select name="mmoc47BdyAttWrkCtMstrForm" property="lineCd" style="width: 150px;">
                        <html:options collection="dropLineCd" labelProperty="label" property="value" />
                    </html:select>
                    <html:errors property='lineCd' />
                </td>
                <th class="SEARCH"><!-- シリーズ --><bean:message key="label.mmoc47.series" /></th>
                <td class="SEARCH">
                    <html:text name="mmoc47BdyAttWrkCtMstrForm" property="series" maxlength="25" size="26"/>
                    <html:errors property='series' />
                </td>
                <th class="SEARCH"><!-- CT登録用仕様 --><bean:message key="label.mmoc47.ct_spec" /></th>
                <td class="SEARCH" nowrap>
                    <html:text name="mmoc47BdyAttWrkCtMstrForm" property="ctSpec" maxlength="4" size="3"/>
                    <html:errors property='ctSpec' />
                </td>
              </tr>
              <tr>
                <th class="SEARCH"><!-- CT登録職場 --><bean:message key="label.mmoc47.ct_workplace" /></th>
                <td class="SEARCH_MUST_INPUT" nowrap>
                    <bean:define id="dropCtWorkPlaceCd" name="mmoc47BdyAttWrkCtMstrForm" property="dropCtWorkPlaceCd" type="java.util.List" />
                    <html:select name="mmoc47BdyAttWrkCtMstrForm" property="ctWorkPlace" style="width: 150px;">
                        <html:options collection="dropCtWorkPlaceCd" labelProperty="label" property="value" />
                    </html:select>
                    <html:errors property='ctWorkPlace' />
                </td>

                <th class="SEARCH"><!-- 世代 --><bean:message key="label.mmoc47.generation" /></th>
                <td class="SEARCH">
                    <html:text name="mmoc47BdyAttWrkCtMstrForm" property="generation" maxlength="25" size="26"/>
                    <html:errors property='generation' />
                </td>
                <th class="SEARCH"><!-- 計画仕向地 --><bean:message key="label.mmoc47.pp_area" /></th>
                <td class="SEARCH" nowrap><html:text name="mmoc47BdyAttWrkCtMstrForm" property="ppArea" maxlength="4" size="3"/>
                    <html:errors property='ppArea' />
                </td>
              </tr>
            </tbody>
          </table>
          <table class="SEARCH">
           <tbody>
              <tr>
                <th class="SEARCH"><!-- 有効日 --><bean:message key="label.mmoc47.from_date" /></th>
                <td class="SEARCH" nowrap><html:text name="mmoc47BdyAttWrkCtMstrForm" property="fromDate" maxlength="10" size="18"/>&nbsp;<bean:message key="label.yyyymmdd_valid" />
                    <html:errors property='fromDate' />
                </td>
                <th class="SEARCH"><!-- 正仮区分 --><bean:message key="label.mmoc47.temp_ctgry" /></th>
                <td class="SEARCH" nowrap>
                    <html:radio name="mmoc47BdyAttWrkCtMstrForm" property="tempCategory" value="0">
                    <!-- 0:正式 --><bean:message key="label.mmoc47.official_value" />
                    </html:radio>
                    <html:radio name="mmoc47BdyAttWrkCtMstrForm" property="tempCategory" value="1">
                    <!-- 1:仮 --><bean:message key="label.mmoc47.provisional_value" />
                    </html:radio>
                    <html:radio name="mmoc47BdyAttWrkCtMstrForm" property="tempCategory" value="*">
                    <!-- *:指定なし--><bean:message key="label.mmoc47.not_specified_value" />
                    </html:radio>
                 </td>

              </tr>
            </tbody>
          </table>
          <table class="SEARCH">
           <tbody>
              <tr>
                <th class="SEARCH"><!-- ソート順 --><bean:message key="label.sort_order" /></th>
                <bean:define id="dropSortCond" name="mmoc47BdyAttWrkCtMstrForm" property="dropSortCond" type="java.util.List" />
                <td class="SEARCH" colspan="5" nowrap>
                    <html:select name="mmoc47BdyAttWrkCtMstrForm" property="sortCond1" style="width: 105px;">
                        <html:options collection="dropSortCond" labelProperty="label" property="value" />
                    </html:select>
                    <html:radio name="mmoc47BdyAttWrkCtMstrForm" property="sortOrd1" value="ASC">
                    <!--  昇順  --><bean:message key="label.ascending" />
                    </html:radio>
                    <html:radio name="mmoc47BdyAttWrkCtMstrForm" property="sortOrd1" value="DESC">
                    <!-- 降順--><bean:message key="label.descending" />
                    </html:radio>

                    <html:select name="mmoc47BdyAttWrkCtMstrForm" property="sortCond2" style="width: 105px;">
                        <html:options collection="dropSortCond" labelProperty="label" property="value" />
                    </html:select>
                    <html:radio name="mmoc47BdyAttWrkCtMstrForm" property="sortOrd2" value="ASC">
                    <!--  昇順  --><bean:message key="label.ascending" />
                    </html:radio>
                    <html:radio name="mmoc47BdyAttWrkCtMstrForm" property="sortOrd2" value="DESC">
                    <!-- 降順--><bean:message key="label.descending" />
                    </html:radio>

                    <html:select name="mmoc47BdyAttWrkCtMstrForm" property="sortCond3" style="width: 105px;">
                        <html:options collection="dropSortCond" labelProperty="label" property="value" />
                    </html:select>
                    <html:radio name="mmoc47BdyAttWrkCtMstrForm" property="sortOrd3" value="ASC">
                    <!--  昇順  --><bean:message key="label.ascending" />
                    </html:radio>
                    <html:radio name="mmoc47BdyAttWrkCtMstrForm" property="sortOrd3" value="DESC">
                    <!-- 降順--><bean:message key="label.descending" />
                    </html:radio>

                    <html:select name="mmoc47BdyAttWrkCtMstrForm" property="sortCond4" style="width: 105px;">
                        <html:options collection="dropSortCond" labelProperty="label" property="value" />
                    </html:select>
                    <html:radio name="mmoc47BdyAttWrkCtMstrForm" property="sortOrd4" value="ASC">
                    <!--  昇順  --><bean:message key="label.ascending" />
                    </html:radio>
                    <html:radio name="mmoc47BdyAttWrkCtMstrForm" property="sortOrd4" value="DESC">
                    <!-- 降順--><bean:message key="label.descending" />
                    </html:radio>

                    <html:select name="mmoc47BdyAttWrkCtMstrForm" property="sortCond5" style="width: 105px;">
                        <html:options collection="dropSortCond" labelProperty="label" property="value" />
                    </html:select>
                    <html:radio name="mmoc47BdyAttWrkCtMstrForm" property="sortOrd5" value="ASC">
                    <!--  昇順  --><bean:message key="label.ascending" />
                    </html:radio>
                    <html:radio name="mmoc47BdyAttWrkCtMstrForm" property="sortOrd5" value="DESC">
                    <!-- 降順--><bean:message key="label.descending" />
                    </html:radio>
                </td>
              </tr>
            </tbody>
          </table>
        </td>
      </tr>

      <!-- 詳細部 -->
      <logic:present name="mmoc47BdyAttWrkCtMstrForm" property="detailList">
        <bean:size id="size" name="mmoc47BdyAttWrkCtMstrForm" property="detailList"/>
        <logic:greaterThan name="size" value="0" >
        <tr>
          <td class="FRAME">
            <table class="LIST" id="searchList">
              <thead>
                <tr>
                  <th class="LIST">
                    <input type="checkbox" name="checkAll" onclick="tickAll();">
                  </th>
                  <th class="LIST">
                    <bean:message key="label.table_header.series" /><br/>
                    <bean:message key="label.table_header.generation" /></th>
                  <th class="LIST">
                    <bean:message key="label.table_header.ct_spec" /></th>
                  <th class="LIST">
                    <bean:message key="label.table_header.pp_area" /></th>
                  <th class="LIST to-hide" style="display: none;">
                    <bean:message key="label.table_header.spec_supp" /></th>
                  <th class="LIST to-hide" style="display: none;">
                    <bean:message key="label.table_header.dest_supp" /></th>
                  <th class="LIST">
                    <input type="button" value=">>" class="BUTTON" onclick="showHide(this);" />
                  </th>
                  <th class="LIST">
                    <bean:message key="label.table_header.appl_mchn" /></th>
                  <th class="LIST">
                    <bean:message key="label.table_header.clss" /></th>
                  <th class="LIST" style="display: none;">
                    <bean:message key="label.table_header.line_cd" /><br/>
                    <bean:message key="label.table_header.ct_workplace" /></th>
                  <th class="LIST">
                    <bean:message key="label.table_header.from_date" /><br/>
                    <bean:message key="label.table_header.to_date" /></th>
                  <th class="LIST">
                    <bean:message key="label.table_header.temp_br_dstnctn" /></th>
                  <th class="LIST">
                    <bean:message key="label.table_header.ct_total_min" /></th>
                  <bean:size id="patternSize" name="mmoc47BdyAttWrkCtMstrForm" property="detailPatternList"/>
                  <logic:greaterThan name="patternSize" value="0" >
                  <logic:iterate id="detailPatternList" name="mmoc47BdyAttWrkCtMstrForm"
                      property="detailPatternList" indexId="index"
                      type="Mmoc47DetailPattern">
                  <bean:define id="colSize" name="mmoc47BdyAttWrkCtMstrForm" property="colSize" type="java.lang.String"/>
                  <logic:lessThan name="index" value='<%= colSize %>' >
                  <th class="LIST">
                    <table>
                      <tr>
                       <th class="LIST">
                         <bean:write name="mmoc47BdyAttWrkCtMstrForm"
                           property='<%="detailPatternList[" + index + "].workplaceCd"%>'/>
                         <html:hidden name = "mmoc47BdyAttWrkCtMstrForm"
                           property='<%="detailPatternList[" + index + "].workplaceCd"%>'/></th>
                      </tr>
                      <tr>
                       <th class="LIST">
                         <bean:write name="mmoc47BdyAttWrkCtMstrForm"
                           property='<%="detailPatternList[" + index + "].operationNo"%>'/>
                         <html:hidden name = "mmoc47BdyAttWrkCtMstrForm"
                           property='<%="detailPatternList[" + index + "].operationNo"%>'/></th>
                      </tr>
                      <tr>
                       <th class="LIST">
                         <bean:write name="mmoc47BdyAttWrkCtMstrForm"
                           property='<%="detailPatternList[" + index + "].opeContents"%>'/>
                         <html:hidden name = "mmoc47BdyAttWrkCtMstrForm"
                           property='<%="detailPatternList[" + index + "].opeContents"%>'/></th>
                      </tr>
                    </table>
                  </th>
                  </logic:lessThan>
                  </logic:iterate>
                  </logic:greaterThan>

                </tr>
              </thead>
              <tbody>
                <logic:iterate id="listItem" name="mmoc47BdyAttWrkCtMstrForm"
                    property="detailList" indexId="index"
                    type="Mmoc47BdyAttWrkCtMstrListItemForm">
                <tr>
                  <td class="LIST" nowrap>
                    <html:errors property='<%="listError" + index.intValue()%>' />
                    <html:checkbox name="mmoc47BdyAttWrkCtMstrForm"
                        property='<%="detailList["+index+"].checked"%>'/>
                  </td>
                  <td class="LIST" nowrap>
                    <bean:write name="mmoc47BdyAttWrkCtMstrForm"
                      property='<%="detailList[" + index + "].series"%>' />
                    <br/>
                    <bean:write name="mmoc47BdyAttWrkCtMstrForm"
                      property='<%="detailList[" + index + "].generation"%>' />
                    <html:hidden name = "mmoc47BdyAttWrkCtMstrForm"
                        property ='<%="detailList[" + index + "].series"%>'/>
                    <html:hidden name = "mmoc47BdyAttWrkCtMstrForm"
                        property ='<%="detailList[" + index + "].generation"%>'/>
                  </td>
                  <td class="LIST" nowrap>
                    <bean:write name="mmoc47BdyAttWrkCtMstrForm"
                      property='<%="detailList[" + index + "].ctSpec"%>'/>
                    <html:hidden name = "mmoc47BdyAttWrkCtMstrForm"
                        property ='<%="detailList[" + index + "].ctSpec"%>'/>
                  </td>
                  <td class="LIST" nowrap>
                    <bean:write name="mmoc47BdyAttWrkCtMstrForm"
                        property='<%="detailList[" + index + "].ppArea"%>'/>
                    <html:hidden name = "mmoc47BdyAttWrkCtMstrForm"
                        property ='<%="detailList[" + index + "].ppArea"%>'/>
                  </td>
                  <td class="LIST to-hide" style="display: none;" nowrap>
                    <bean:write name="mmoc47BdyAttWrkCtMstrForm"
                        property='<%="detailList[" + index + "].specSupp"%>'/>
                    <html:hidden name = "mmoc47BdyAttWrkCtMstrForm"
                        property ='<%="detailList[" + index + "].specSupp"%>'/>
                  </td>
                  <td class="LIST to-hide" style="display: none;" nowrap>
                    <bean:write name="mmoc47BdyAttWrkCtMstrForm"
                        property='<%="detailList[" + index + "].destSupp"%>'/>
                    <html:hidden name = "mmoc47BdyAttWrkCtMstrForm"
                        property ='<%="detailList[" + index + "].destSupp"%>'/>
                  </td>
                  <td class="LIST" nowrap>&nbsp;</td>
                  <td class="LIST_NUMBER" nowrap>
                    <bean:write name="mmoc47BdyAttWrkCtMstrForm"
                        property='<%="detailList[" + index + "].applMachineCd"%>'/>
                    <html:hidden name = "mmoc47BdyAttWrkCtMstrForm"
                        property ='<%="detailList[" + index + "].applMachineCd"%>'/>
                  </td>
                  <td class="LIST" nowrap>
                    <bean:write name="mmoc47BdyAttWrkCtMstrForm"
                        property='<%="detailList[" + index + "].classCd"%>'/>
                    <html:hidden name = "mmoc47BdyAttWrkCtMstrForm"
                        property ='<%="detailList[" + index + "].classCd"%>'/>
                  </td>
                  <td class="LIST" nowrap style="display: none;">
                    <bean:write name="mmoc47BdyAttWrkCtMstrForm"
                        property='<%="detailList[" + index + "].lineCd"%>'/>
                    <br/>
                    <bean:write name="mmoc47BdyAttWrkCtMstrForm"
                        property='<%="detailList[" + index + "].ctWorkPlace"%>'/>

                    <html:hidden name = "mmoc47BdyAttWrkCtMstrForm"
                        property ='<%="detailList[" + index + "].lineCd"%>'/>
                    <html:hidden name = "mmoc47BdyAttWrkCtMstrForm"
                        property ='<%="detailList[" + index + "].ctWorkPlace"%>'/>
                  </td>
                  <td class="LIST_DATE" nowrap>
                    <bean:write name="mmoc47BdyAttWrkCtMstrForm"
                        property='<%="detailList[" + index + "].fromDate"%>'/>
                    <br/>
                    <bean:write name="mmoc47BdyAttWrkCtMstrForm"
                        property='<%="detailList[" + index + "].toDate"%>'/>

                    <html:hidden name = "mmoc47BdyAttWrkCtMstrForm"
                        property ='<%="detailList[" + index + "].fromDate"%>'/>
                    <html:hidden name = "mmoc47BdyAttWrkCtMstrForm"
                        property ='<%="detailList[" + index + "].toDate"%>'/>
                  </td>
                  <td class="LIST" nowrap>
                    <bean:write name="mmoc47BdyAttWrkCtMstrForm"
                        property='<%="detailList[" + index + "].tempCategory"%>'/>
                     <html:hidden name = "mmoc47BdyAttWrkCtMstrForm"
                        property ='<%="detailList[" + index + "].tempCategory"%>'/>
                  </td>
                  <td class="LIST_NUMBER" nowrap>
                    <bean:write name="mmoc47BdyAttWrkCtMstrForm"
                        property='<%="detailList[" + index + "].ctSum"%>'/>
                     <html:hidden name = "mmoc47BdyAttWrkCtMstrForm"
                        property ='<%="detailList[" + index + "].ctSum"%>'/>
                  </td>

                  <logic:iterate id="detailValueList" name="mmoc47BdyAttWrkCtMstrForm"
                      property='<%="detailList[" + index + "].detailValueList"%>'
                      indexId="index2" type="Mmoc47DetailValue">
                  <bean:define id="colSize" name="mmoc47BdyAttWrkCtMstrForm" property="colSize" type="java.lang.String"/>
                  <logic:lessThan name="index2" value='<%= colSize %>' >
                  <td class="LIST_NUMBER" nowrap>
                     <bean:write name="mmoc47BdyAttWrkCtMstrForm"
                       property='<%="detailList[" + index + "].detailValueList[" + index2 + "].ctSum"%>'/>
                     <html:hidden name="mmoc47BdyAttWrkCtMstrForm"
                       property='<%="detailList[" + index + "].detailValueList[" + index2 + "].ctSum"%>'/>
                     <html:hidden name="mmoc47BdyAttWrkCtMstrForm"
                       property='<%="detailList[" + index + "].detailValueList[" + index2 + "].workplaceCd"%>'/>
                     <html:hidden name="mmoc47BdyAttWrkCtMstrForm"
                       property='<%="detailList[" + index + "].detailValueList[" + index2 + "].operationNo"%>'/>
                     <html:hidden name="mmoc47BdyAttWrkCtMstrForm"
                       property='<%="detailList[" + index + "].detailValueList[" + index2 + "].opeContents"%>'/>
                  </td>
                  </logic:lessThan>
                  </logic:iterate>

                </tr>
                </logic:iterate>
              </tbody>
              <tfoot>
                  <tr>
                  <td colspan="9">
                    <logic:equal name="mmoc47BdyAttWrkCtMstrForm" property="enabButtonPrev" value="false">
                      <input type="button" value='<bean:message key="label.prev" />' class="BUTTON" disabled>
                    </logic:equal>
                    <logic:equal name="mmoc47BdyAttWrkCtMstrForm" property="enabButtonPrev" value="true">
                      <input type="button" value='<bean:message key="label.prev" />' class="BUTTON" onClick = "submitProcess('prev')">
                    </logic:equal>
                      <bean:write name="mmoc47BdyAttWrkCtMstrForm" property="pageRatio"/>&nbsp;<bean:message key="label.page" />
                    <logic:equal name="mmoc47BdyAttWrkCtMstrForm" property="enabButtonNext" value="false">
                      <input type="button" value='<bean:message key="label.next" />' class="BUTTON" disabled>
                    </logic:equal>
                    <logic:equal name="mmoc47BdyAttWrkCtMstrForm" property="enabButtonNext" value="true">
                      <input type="button" value='<bean:message key="label.next" />' class="BUTTON" onClick = "submitProcess('next')">
                    </logic:equal>
                  </td>
                </tr>
              </tfoot>
            </table>
          </td>
        </tr>
        </logic:greaterThan>
      </logic:present>
      <html:hidden name="mmoc47BdyAttWrkCtMstrForm" property="zoneCd" />
      <html:hidden name="mmoc47BdyAttWrkCtMstrForm" property="headLangCd" />
      <html:hidden name="mmoc47BdyAttWrkCtMstrForm" property="localeLangCd" />
      <html:hidden name="mmoc47BdyAttWrkCtMstrForm" property="process" />

      <html:hidden name="mmoc47BdyAttWrkCtMstrForm" property="hiddenLineCd" />
      <html:hidden name="mmoc47BdyAttWrkCtMstrForm" property="hiddenSeries" />
      <html:hidden name="mmoc47BdyAttWrkCtMstrForm" property="hiddenCtSpec" />
      <html:hidden name="mmoc47BdyAttWrkCtMstrForm" property="hiddenCtWorkPlace" />
      <html:hidden name="mmoc47BdyAttWrkCtMstrForm" property="hiddenGeneration" />
      <html:hidden name="mmoc47BdyAttWrkCtMstrForm" property="hiddenPpArea" />
      <html:hidden name="mmoc47BdyAttWrkCtMstrForm" property="hiddenFromDate" />
      <html:hidden name="mmoc47BdyAttWrkCtMstrForm" property="hiddenTempCategory" />

      <html:hidden name="mmoc47BdyAttWrkCtMstrForm" property="hiddenSortCond1" />
      <html:hidden name="mmoc47BdyAttWrkCtMstrForm" property="hiddenSortCond2" />
      <html:hidden name="mmoc47BdyAttWrkCtMstrForm" property="hiddenSortCond3" />
      <html:hidden name="mmoc47BdyAttWrkCtMstrForm" property="hiddenSortCond4" />
      <html:hidden name="mmoc47BdyAttWrkCtMstrForm" property="hiddenSortCond5" />
      <html:hidden name="mmoc47BdyAttWrkCtMstrForm" property="hiddenSortOrd1" />
      <html:hidden name="mmoc47BdyAttWrkCtMstrForm" property="hiddenSortOrd2" />
      <html:hidden name="mmoc47BdyAttWrkCtMstrForm" property="hiddenSortOrd3" />
      <html:hidden name="mmoc47BdyAttWrkCtMstrForm" property="hiddenSortOrd4" />
      <html:hidden name="mmoc47BdyAttWrkCtMstrForm" property="hiddenSortOrd5" />

      <html:hidden name="mmoc47BdyAttWrkCtMstrForm" property="pageSize" />
      <html:hidden name="mmoc47BdyAttWrkCtMstrForm" property="hiddenPageSize" />
      <html:hidden name="mmoc47BdyAttWrkCtMstrForm" property="totalCount" />
      <html:hidden name="mmoc47BdyAttWrkCtMstrForm" property="currentPage" />
      <html:hidden name="mmoc47BdyAttWrkCtMstrForm" property="totalPage" />

    </table>
</html:form>

<!-- システム共通フッター -->
<%@ include file="/include/footer.jsp"%>
