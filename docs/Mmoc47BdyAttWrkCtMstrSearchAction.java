import java.util.Locale;
import java.util.Properties;

import javax.naming.Context;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionError;
import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.util.MessageResources;

public class Mmoc47BdyAttWrkCtMstrSearchAction extends MmAction {

    /**
     *
     * @param actionMapping このインスタンスを選択するために使用したActionMapping
     * @param actionForm 存在するならば、このリクエストのためのActionForm Bean
     * @param request  処理しているServletリクエスト
     * @param response 処理しているServletレスポンス
     * @param context  Context EJB Lookup Context
     * @return ActionForward
     * @throws Exception 例外が発生した
     */
    public ActionForward doProcess(ActionMapping actionMapping, ActionForm actionForm,
        HttpServletRequest request, HttpServletResponse response, Context context)
        throws Exception {

        ActionForward actionForward = null;
        Mmoc47BdyAttWrkCtMstrForm form = (Mmoc47BdyAttWrkCtMstrForm) actionForm;
        Locale locale = super.getLocale(request);
        MmUser user = super.getMmUser(request, context);
        MessageResources resource = super.getResources();
        Properties property = super.getProperties();

        form.setResources(resource);
        form.setLocaleLangCd(WebUtil.getLangCd(locale));
        form.setZoneCd(user.getZoneCd());
        form.setHeadLangCd((String) super.getAttribute(request, "headDefLangCd"));
        form.setUserId(user.getUserId());
        ActionErrors errors = new ActionErrors();

        Mmoc47BdyAttWrkCtMstrValue value = new Mmoc47BdyAttWrkCtMstrValue();
        Mmoc47BdyAttWrkCtMstrValueUtil.transferFormToValue(form, value);

        try {
            value = (Mmoc47BdyAttWrkCtMstrValue) super.invokeService(
                    "jp.co.scm.mm.business.server.mmoc.Mmoc47BdyAttWrkCtMstrService",
                    "getDropdownContent", value, context);
            form.setDropLineCd(value.getDropLineCd());
            form.setDropCtWorkPlaceCd(value.getDropCtWorkPlaceCd());
        } catch (ApplicationException sae) {
            errors = super.createErrorMessage(sae, locale);
            super.saveErrors(request, errors);

            return actionMapping.findForward("failure");
        } finally {
            form.setDropLineCd(value.getDropLineCd());
            form.setDropCtWorkPlaceCd(value.getDropCtWorkPlaceCd());
        }

        errors = form.validate(actionMapping, request);

        if (errors != null) {
            errors.add("result", new ActionError("error.top", "" + errors.size()));
            super.saveErrors(request, errors);

            return actionMapping.findForward("failure");
        }

        try {
            // 検索条件が変更チェック
            searchConditionsChangedCheck(form);

            // 一覧行数の最小限と最大限を取得します
            int min = ConverterUtil.toInt(property.getProperty("MMOC47_minList"));
            // 適当な値を表示件数に設定
            int displayLines = Math.min(Math.max(ConverterUtil.toInt(form.getPageSize()), min),
                    100);
            form.setPageSize(ConverterUtil.toString(displayLines));

            // 一覧行数の最小限と最大限を取得します
            int minColList = ConverterUtil.toInt(property.getProperty("MMOC47_colList"));
            // 適当な値を表示件数に設定
            int displayCols = Math.min(Math.max(ConverterUtil.toInt(form.getColSize()), minColList),
                    100);
            form.setColSize(ConverterUtil.toString(displayCols));

            value.setProperties(super.getProperties());
            Mmoc47BdyAttWrkCtMstrValueUtil.transferFormToValue(form, value);

            // ページング用変数値をバリューに設定
            setPagingPropertiesToValue(form, value, property);

            value = (Mmoc47BdyAttWrkCtMstrValue) super.invokeService(
                "Mmoc47BdyAttWrkCtMstrService",
                "search", value, context);

            Mmoc47BdyAttWrkCtMstrValueUtil.transferValueToForm(value, form);

            // ページング用変数値をバリューに設定
            setPagingPropertiesToForm(value, form);

            // 検索条件をHiddenフィールドにコピー
            form.copyOriginal();

            actionForward = actionMapping.findForward("success");

        } catch (ApplicationException sae) {
            errors = super.createErrorMessage(sae, locale);
            super.saveErrors(request, errors);
            actionForward = actionMapping.findForward("failure");
        }

        return actionForward;
    }

    /**
     * 検索条件が変更チェック
     *
     * @param form フォーム
     * @throws ApplicationException 一覧行数、検索キーが変更された場合
     */
    private void searchConditionsChangedCheck(Mmoc47BdyAttWrkCtMstrForm form)
            throws ApplicationException {

        // 処理が次ページ、前ページの場合、検索条件変更チェックを行う
        if("next".equals(form.getProcess())
                || "prev".equals(form.getProcess()) ){

            // 検索キーが変更された時、エラーにする
            if(form.isSearchKeyChanged()){
                // ＥＲＲ９９９：ｋｅｙ項目が変わりました。再度照会処理を行ってください。
                throw new ApplicationException("error.changed.primary.keys");
            }

            // 表示件数が変更された時、エラーにする
            if(!form.getPageSize().equals(form.getHiddenPageSize())){
                // ＥＲＲ９９７：一覧行数を変更したい時、再度照会処理を行ってください。
                throw new ApplicationException("error.changed.list.size");
            }
        }
    }

    /**
     * ページング用のフォーム値をバリューにコピーする。
     *
     * @param form      データ元のフォームクラス
     * @param value     データコピー先のバリュークラス
     */
    private void setPagingPropertiesToValue(Mmoc47BdyAttWrkCtMstrForm form,
            Mmoc47BdyAttWrkCtMstrValue value, Properties property) {

        /*
         * 【注意】
         *  SearchValue#setPageSize()を呼ぶ前に、サブシステムのPropertyを設定する必要がある。
         *  設定しなければ、NullPointerExceptionが発生する。
         */
        value.setProperties(property);

        value.setProcess(form.getProcess());
        value.setPageSize(ConverterUtil.toInt(form.getPageSize()));
        value.setCurrentPage(ConverterUtil.toInt(form.getCurrentPage()));
        value.setTotalCount(ConverterUtil.toInt(form.getTotalCount()));

    }

    /**
     * ページング用のバリュー値をフォームにセットする。
     *
     * @param value     データ元のバリュークラス
     * @param form      データコピー先フォームクラス
     */
    private void setPagingPropertiesToForm(Mmoc47BdyAttWrkCtMstrValue value,
            Mmoc47BdyAttWrkCtMstrForm form) {

        form.setPageSize(ConverterUtil.toString(value.getPageSize()));
        form.setCurrentPage(ConverterUtil.toString(value.getNewCurrentPage()));
        form.setTotalPage(ConverterUtil.toString(value.getTotalPage()));
        form.setTotalCount(ConverterUtil.toString(value.getTotalCount()));

    }

}
