Welcome to Tech Fridays
===============================

Wicked tool to cover awesome topics

###1. Technologies used
* Servlet 3.0+ container, like Tomcat 7 or Jetty 8
* Gradle 2.0
* Spring Spring 4.1.6.RELEASE
* JSTL 1.2
* Logback 1.1.3
* Boostrap 3

###2. To Run this project locally
```shell
$ git clone git@bitbucket.org:jsalatan/techfri.git
$ gradle jettyRun
```
Access ```http://localhost:8080/techfri```

###3. To import this project into Eclipse IDE
1. ```$ gradle eclipse```
2. Import into Eclipse via **existing projects into workspace** option.
3. Done.