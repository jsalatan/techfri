package com.awsys.techfri.config;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

@Configuration
@ComponentScan({ "com.awsys.techfri.service",  "com.awsys.techfri.repository"})
public class SpringRootConfig {
}